(in-package #:fouric)

(deftype pathname-designator ()
  '(or string
    pathname
    ;;file-associated-stream
    ))

(deftype function-designator ()
  '(or symbol
    function))

(deftype string-designator ()
  '(or character
    symbol
    string))

;; convenience alias for DECLAIM FTYPE
(defmacro ft (name (&rest arg-types) return-type)
  "function type"
  `(declaim (ftype (function (,@arg-types) ,return-type) ,name)))

(defmacro make-the-instance (class &rest initargs)
  "convenience macro around MAKE-INSTANCE that adds some type declarations"
  (let ((ret (gensym)))
    `(let ((,ret (make-instance ,class ,@initargs)))
       (the ,(if (and (listp class)
                      (eq (first class) 'quote))
                 (second class)
                 class) ,ret)
       ,ret)))

(deftype just (type)
  `(values ,type &optional))

;; TODO: allow optional to take multiple types, and OR them together with a null
(deftype optional (type)
  `(or ,type null))

(deftype nothing ()
  '(values &optional))
