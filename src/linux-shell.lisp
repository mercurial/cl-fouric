(in-package #:fouric)

;; TODO: need to figure out whether to continue using these split path sequences or whether to use native CL pathspecs

(ql:quickload :split-sequence)
(import 'split-sequence:split-sequence)
;; TODO: need to quote before sending to shell
(ql:quickload :trivial-shell)
(import 'trivial-shell:shell-command)

(defparameter home (remove "" (split-sequence #\/ (namestring (user-homedir-pathname))) :test #'string=))

(defun ensure-list (thing)
  (if (listp thing)
      thing
      (list thing)))

(defun path (&rest things)
  ;; TODO: coerce numbers to strings
  ;; TODO: search strings for forward slashes and split
  (apply #'append (mapcar #'ensure-list things)))

(defun quote-path (path)
  (ppcre:regex-replace-all "( )" path "\\ "))

;; do we need to quote these?
(defun to-linux-path (pathlist)
  (format nil "~{/~A~}" pathlist))
(defun add-slash (string)
  (concatenate 'string string "/"))

(defun ensure-linux-path (path)
  (etypecase path
    (list
     (to-linux-path path))
    (string
     path)))

(defun newlines->list (string)
  (split-sequence #\newline string))

(defun ls (path)
  (remove "" (newlines->list (shell-command (format nil "ls ~a" (ensure-linux-path path)))) :test #'string=))

(defun exists? (filename)
  (probe-file (ensure-linux-path filename)))
(defun is-file? (filename)
  (and (exists? filename) (let* ((path (ensure-linux-path filename))
                                 (containing-directory (directory-namestring path))
                                 (sibling-files (mapcar #'file-namestring (uiop:directory-files containing-directory)))
                                 (name (file-namestring path)))
                            (when (member name sibling-files :test #'string=)
                              name))))

(defun inbox-wrapper ()
  (let ((argv ))))

;; TODO: define homedir as split path
(defun inbox (command &rest args)
  (let* ((dropbox (path home "Dropbox"))
         (evr (path dropbox "Apps" "Easy Voice Recorder"))
         (tmp (path dropbox "tmp"))
         (inbox (path home "mercurial" "inbox.txt")))
    (ecase command
      (pfdb
       (let ((dropbox-inbox (path tmp "inbox.txt")))
         (if (is-file? dropbox-inbox)
             (format t "inbox is a file~%")
             (error "inbox does not exist at ~s~%" dropbox-inbox)))))))

;;(defun filext? (file ext &rest more-exts))

;; directory as list, or plist/alist/hashtable?

(defclass ls-entry ()
  ((name :accessor name :initarg :name)
   (parent :accessor parent :initarg :parent)
   (kind :accessor kind :initarg :kind)))
(defun ls-files (path)
  (loop :for fullpath :in (uiop:directory-files (add-slash (to-linux-path path)))
        :collect (let ((name (file-namestring fullpath))
                       (parent (directory-namestring fullpath)))
                   (make-instance 'ls-entry :name name :parent parent :kind :file))))
(uiop:directory-files (add-slash (to-linux-path home)))
;; https://lispcookbook.github.io/cl-cookbook/files.html#listing-files-in-a-directory
(defun pathname->)

#|
(defstruct ls-entry
name
kind)

(defun ls ()
(with-directory-iterator (next (current-directory))
(iter (for entry = (next))
(while entry)
(collect (make-ls-entry :name (namestring entry)
:kind (file-kind entry))))))

(defun match (regex to-search &key key)
(let ((target (if key
(funcall key to-search)
to-search)))
(ppcre:scan regex target)))

;; Regular Expression Filter
(defun ref (regex haystack &key key)
(mapcar (lambda (item) (match regex item :key key)) haystack))

(defgeneric grep-key (item))
(defmethod grep-key ((item ls-entry))
(name item))
(defmethod grep-key ((item string))
item)
(defun grep (regex haystack)
(mapcar (lambda (item) (match regex (grep-key item))) haystack))

;; ls | rg 'lisp$'
(remove-if-not (lambda (x) (ends-with-subseq "lisp" (slot-value x 'name))) (ls))
(ref "$lisp" (ls) :key #'name)
(grep "$lisp" (ls))
|#
