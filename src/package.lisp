(push :fouric *features*)

(defpackage #:fouric
  (:use #:cl)
  (:local-nicknames (:a :alexandria))
  (:export
   #:read-file
   #:file-lines
   #:write-lines
   #:write-file
   #:with-init-window-gl
   #:prof
   #:resource
   #:map-files
   #:doitimes
   #:while
   #:fn-case
   #:clampf
   #:inclampf
   #:+-clamp
   #:pushlast
   #:with-accessors+
   #:with-accessors++
   #:replace-texture
   #:*whitespace-characters*
   #:eql/package-relaxed
   #:edit-definition
   #:emacs-jump-to-term
   #:intersperse
   #:unix-to-universal-time
   #:update-swank

   ;; macros
   #:udefun
   #:cond?
   #:out
   #:on-global-update
   #:retry-once
   #:let-ret1
   #:with-accumulator
   #:when-case

   #:nmember
   #:nonconsecutive-substring-match
   #:emacs-eval
   #:genspaces

   ;; charms
   #:eclamp
   #:out-of-bounds
   #:with-color
   #:draw-string
   #:draw-char
   #:draw
   #:colors-to-index
   #:normalize-color
   #:clear-window
   #:write-spaces-window
   #:refresh-window
   #:get-char
   #:init-charms
   #:with-charms
   #:without-charms
   #:restart-charms
   #:update-dimensions
   #:with-invert
   #:window
   #:charms-width
   #:charms-height
   #:height
   #:width
   #:color?
   #:*invert*
   #:*silently-clip*
   #:*foreground*
   #:*background*
   #:terminal-handle
   #:draw-box

   ;; narrower
   #:narrower
   #:make-narrower
   #:criteria
   #:candidates
   #:results
   #:haystack
   #:needle
   #:needle-append
   #:needle-backspace
   #:needle-clear

   ;; types
   #:pathname-designator
   #:function-designator
   #:string-designator
   #:ft
   #:make-the-instance
   #:just
   #:optional
   #:nothing

   ;; misc
   #:command
   #:ensure-gethash
   #:defn

   #:run-tests
   #:run-all-tests
   #:add-test
   #:remove-test
   #:list-tests
   #:clear-tests
   #:rove-notify-test
   ))
